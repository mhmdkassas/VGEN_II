import sqlite

struct App {
	pub mut :
		database sqlite.DB
}

struct Opcode {
	id int 	[primary; sql:serial]
	mnemonic string
	architecture string
	function string
	size int
	exceptions string // will become []string
	format string
}

fn (app &App) find_all_articles() []Opcode {
	return sql app.database { 
		select from Opcode
	}
}

fn main() {
	// mut database := sqlite.connect('test.db') or {panic(err)}
	// mut columns := ['ID INT PRIMARY KEY NOT NULL', 'TYPE TEXT', 'SIZE INT']
	// database.create_table('opcodes', columns)
	// mut r1 := orm.QueryData {
	// 		fields : ['ID', 'TYPE', 'SIZE']
	// 		data : ['10', 'read', '32']
	// }
	// database.insert('opcodes', r1) or {panic(err)}
	// print(database.exec('SELECT * from opcodes'))

	mut app := App {
		database : sqlite.connect('memory.db') or { panic(err) }
	}

	sql app.database {
		create table Opcode
	}

	first_op := Opcode {
		mnemonic: 'a1'
		architecture: 'a2'
		function: 'a3'
		size: 32
		exceptions: 'a5'
		format: 'a6'
	}

	second_op := Opcode {
		mnemonic: 'b1'
		architecture: 'b2'
		function: 'b3'
		size: 64
		exceptions: 'b5'
		format: 'b6'
	}

	third_op := Opcode {
		mnemonic: 'c1'
		architecture:  'c2'
		function: 'c3'
		size: 32
		exceptions: 'c5'
		format: 'c6'
	}

	// sql app.database {
	// 	insert first_op into Opcode
	// 	insert second_op into Opcode
	// 	insert third_op into Opcode
	// }
	print(app.find_all_articles())
}