module unittests
import test
import reg

// Normal Case
// Add functionality comments to all tests
pub fn m1_test1() {
	mut input := [3, 2, 1, 1]
	mut tool := test.Tools{}
	mut regs := tool.get_rand_reg_map(input[0], input[1], input[2], input[3]) or {panic(err)}
	
	true_regs := reg.RandRegMap {
		source : input[0]
		dest : input[1]
		source_dest : input[2]
		base : input[3]
	}

	if true_regs == regs {
		println('SUCCESS: Test1 M1')
	}
	else {
		println('FAIL: Test1 M1')
	}
}

pub fn m1_test2(){
	mut input := [0, 0, 0, 0]
	mut tool := test.Tools{}
	mut regs := tool.get_rand_reg_map(input[0], input[1], input[2], input[3]) or {reg.RandRegMap {
																					source : -5000
																				 }}
	if regs.source == -5000 {
		println('SUCCESS: Test2 M1')
	}
	else {
		println('FAIL: Test2 M1')
	}
}

pub fn m2_test1(){
	mut input := [['s1', 's2', 's3'], ['d1', 'd2', 'd3'], ['sd1', 'sd2'], ['b1']]
	mut tool := test.Tools{}
	mut regs := tool.get_reg_map(input[0], input[1], input[2], input[3]) or {panic(err)}
	
	true_regs := reg.RegMap {
		source : input[0]
		dest : input[1]
		source_dest : input[2]
		base : input[3]
	}

	if true_regs == regs {
		println('SUCCESS: Test1 M2')
	}
	else {
		println('FAIL: Test1 M2')
	}

}

pub fn m2_test2(){
	mut input := [[''], [''], [''], ['']]
	mut tool := test.Tools{}
	mut regs := tool.get_reg_map(input[0], input[1], input[2], input[3]) or {reg.RegMap {
																					source : ['-5000']
																				 }}
	if regs.source == ['-5000'] {
		println('SUCCESS: Test2 M2')
	}
	else {
		println('FAIL: Test2 M2')
	}
}


