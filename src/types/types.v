module types

import reg
import filter

pub type Filters = filter.Target | filter.Restriction

pub type Regs = reg.RandRegMap | reg.RegMap