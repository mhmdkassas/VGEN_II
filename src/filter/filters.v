module filter

pub struct Target {
	seq []string
	frag []string
	instr []string
	op []string
}

pub struct Restriction {
	seq []string
	frag []string
	instr []string
	op []string
}

pub type Filter = Target | Restriction