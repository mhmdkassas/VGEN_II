module db
import sqlite

pub struct App2 {
	pub mut : 
		database sqlite.DB
}

struct DatabaseRecord {
	id int [primary; sql:serial]
	mnemonic string
	func string
	subfunc string
	size string
	unsign string // unsigned seems to be a reserved word (avoid using)
	precision string
	format string
	source_base string
	source1 string
	source2 string
	source3 string
	source_csr string
	rm string
	dest string
	dest_csr string
	base_imm string
	imm1 string
	imm2 string
	imm3 string
	arch string
	ext string
	exc string
}

pub struct FilterMap_old {
	id_tags []string = ['NOTHING']
	logic0 string = 'NOTHING'
	mnemonic_tags []string = ['NOTHING']
	logic1 string = 'NOTHING'
	func_tags []string = ['NOTHING']
	logic2 string = 'NOTHING'
	subfunc_tags []string = ['NOTHING']
	logic3 string = 'NOTHING'
	size_tags []string = ['NOTHING']
	logic4 string = 'NOTHING'
	unsigned_tags []string = ['NOTHING']
	logic5 string = 'NOTHING'
	precision_tags []string = ['NOTHING']
	logic6 string = 'NOTHING'
	format_tags []string = ['NOTHING']

	logic7 string = 'NOTHING'


	source_base []string = ['NOTHING']
	logic8 string = 'NOTHING'
	source1_tags []string = ['NOTHING']
	logic9 string = 'NOTHING'
	source2_tags []string = ['NOTHING']
	logic10 string = 'NOTHING'
	source3_tags []string = ['NOTHING']
	logic11 string = 'NOTHING'
	source_csr_tags []string = ['NOTHING']

	logic12 string = 'NOTHING'

	rm_tags []string = ['NOTHING']
	logic13 string = 'NOTHING'
	dest_tags []string = ['NOTHING']
	logic14 string = 'NOTHING'
	dest_csr_tags []string = ['NOTHING']

	logic15 string = 'NOTHING'

	base_imm_tags []string = ['NOTHING']
	logic16 string = 'NOTHING'
	imm1_tags []string = ['NOTHING']
	logic17 string = 'NOTHING'
	imm2_tags []string = ['NOTHING']
	logic18 string = 'NOTHING'
	imm3_tags []string = ['NOTHING']

	logic19 string = 'NOTHING'

	arch_tags []string = ['NOTHING']
	logic20 string = 'NOTHING'
	ext_tags []string = ['NOTHING']
	logic21 string = 'NOTHING'
	exc_tags []string = ['NOTHING']
}

pub struct FilterMap {
	id_tags string = 'NOTHING'
	logic0 string = 'NOTHING'

	mnemonic_tags string = 'NOTHING'
	logic1 string = 'NOTHING'

	func_tags string = 'NOTHING'
	logic2 string = 'NOTHING'

	subfunc_tags string = 'NOTHING'
	logic3 string = 'NOTHING'

	size_tags string = 'NOTHING'
	logic4 string = 'NOTHING'

	unsigned_tag bool = false
	logic5 string = 'NOTHING'

	precision_tags string = 'NOTHING'
	logic6 string = 'NOTHING'

	arch_tags string = 'NOTHING'
	logic7 string = 'NOTHING'

	ext_tags string = 'NOTHING'
	logic8 string = 'NOTHING'

	exc_tags string = 'NOTHING'
	logic9 string = 'NOTHING'

	rm_tag bool = false
	logic10 string = 'NOTHING'

	source_tags string = 'NOTHING'
	logic11 string = 'NOTHING'

	dest_tags string = 'NOTHING'
	logic12 string = 'NOTHING'

	imm_tags string = 'NOTHING'
	logic13 string = 'NOTHING'
}

// calls the encode functions for each field in FilterMap
// Each encode function returns a string which will be a 
// part of the query string that will be used to filter the
// instruction database.
// Each encode function is responsible for its trailing spaces
pub fn (f &FilterMap) encode() string {
	id_query := encode_type_a(f.id_tags, "id") or {panic(err)}
	logic0 := encode_logic(f.logic0) or {panic(err)}

	mnemonic_query := encode_type_a(f.mnemonic_tags, "mnemonic") or {panic(err)}
	logic1 := encode_logic(f.logic1) or {panic(err)}

	func_query := encode_type_a(f.func_tags, "func") or {panic(err)}
	logic2 := encode_logic(f.logic2) or {panic(err)}

	subfunc_query := encode_type_a(f.subfunc_tags, "subfunc") or {panic(err)}
	logic3 := encode_logic(f.logic3) or {panic(err)}

	size_query := encode_type_a(f.size_tags, "size") or {panic(err)}
	logic4 := encode_logic(f.logic4) or {panic(err)}

	unsigned_query := encode_type_b(f.unsigned_tag, "unsign")
	logic5 := encode_logic(f.logic8) or {panic(err)}

	precision_query := encode_type_a(f.precision_tags, "precision") or {panic(err)}
	logic6 := encode_logic(f.logic5) or {panic(err)}

	arch_query := encode_type_arch(f.arch_tags) or {panic(err)}
	logic7 := encode_logic(f.logic7) or {panic(err)}

	ext_query := encode_type_a(f.ext_tags, "ext") or {panic(err)}
	logic8 := encode_logic(f.logic6) or {panic(err)}
	
	exc_query := encode_type_a(f.exc_tags, "exc") or {panic(err)}
	logic9 := encode_logic(f.logic7) or {panic(err)}

	rm_query := encode_type_b(f.rm_tag, "rm") 
	logic10 := encode_logic(f.logic10) or {panic(err)}

	source_query := encode_type_source(f.source_tags)
	logic11 := encode_logic(f.logic11) or {panic(err)}

	dest_query := encode_type_dest(f.dest_tags)
	logic12 := encode_logic(f.logic12) or {panic(err)}

	imm_query := encode_type_imm(f.imm_tags)
	logic13 := encode_logic(f.logic13) or {panic(err)}

	return id_query + logic0 + mnemonic_query + logic1 + func_query + logic2 + subfunc_query + logic3 + size_query + logic4
					+ unsigned_query + logic5 + precision_query + logic6 + arch_query + logic7 + ext_query + logic8 + exc_query
					+ logic9 + rm_query + logic10 + source_query + logic11 + dest_query + logic12 + imm_query + logic13
}

// encodes the logic operators into the query string
pub fn encode_logic(tag string) ?string {
	if tag == "NOTHING" {
		return ""
	}
	else if tag == "||"{
		return "OR "
	}
	else if tag == "&" {
		return "AND "
	}
	else {
		panic('Incorrect LOGIC operator. Valid inputs are & and ||.')
	}
}

pub fn encode_type_a(tags string, field string) ?string {
	mut app := App2 {
		database : sqlite.connect('instructionDB.db') or {panic(err)}
	}

	if tags == 'NOTHING' {
		return ""
	}
	else{
		mut words := []string
		mut word  := ""
		for c in tags{
			if c.ascii_str() == ' '{
				words << word
				word = ""
			}
			else {
				word = word + c.ascii_str()
				// println(word)
			}
		}
		words << word
		mut query := ""
		for w in words {
			if w == "||" {
				query = query + "OR "
			}
			else {
				mut parameter_eqn := field + "=" + "'" + w + "'" + " "
				records, _ := app.sel(parameter_eqn)
				if records.len == 0 {
					panic(w + " does not exist in " + field + " column")
				}
				query = query + parameter_eqn
			}
		}
		return "(" + query + ")"
	}
}

pub fn encode_type_b(tag bool, field string) string {
	if tag == false {
		return ""
	}
	else {
		parameter_eqn := field + "!=''"	
		return "(" + parameter_eqn + ")"
	}
}

pub fn encode_type_source(tag string)  string {
	if tag == 'NOTHING' {
		return ""
	}
	else {
		mut words := []string
		mut word := ""
		for c in tag {
			if c.ascii_str() == ' '{
				words << word
				word = ""
			}
			else {
				word = word + c.ascii_str()
			}
		}
		words << word
		mut query := ""
		for w in words {
			if w == "||" {
				query = query + "OR "
			}
			else if w == "&" {
				query = query + "AND "
			}
			else if w == "hasCSR"{
				query = query + "source_csr != ''"
			}
			else if w == "hasBase" {
				query = query + "source_base != ''"
			}
			else if w == "0" {
				query = query + "(source_base='' AND source1='' AND source2='' AND source3='' AND source4=''"
			}
			else if w == "1" {
				query = query + 
						"((source_base!='' AND source1='' AND source2='' AND source3='' AND source4='') OR
						  (source_base='' AND source1!='' AND source2='' AND source3='' AND source4='') OR
						  (source_base='' AND source1='' AND source2!='' AND source3='' AND source4='') OR
						  (source_base='' AND source1='' AND source2='' AND source3!='' AND source4='') OR
						  (source_base='' AND source1='' AND source2='' AND source3='' AND source4!='')"
			}
			else if w == "2" {
				query = query + 
						"((source_base!='' AND source1!='' AND source2='' AND source3='' AND source4='') OR
						  (source_base!='' AND source1='' AND source2!='' AND source3='' AND source4='') OR
						  (source_base!='' AND source1='' AND source2='' AND source3!='' AND source4='') OR
						  (source_base!='' AND source1='' AND source2='' AND source3='' AND source4!='') OR
						  (source_base='' AND source1!='' AND source2!='' AND source3='' AND source4='') OR
						  (source_base='' AND source1!='' AND source2='' AND source3!='' AND source4='') OR
						  (source_base='' AND source1!='' AND source2='' AND source3='' AND source4!='') OR
						  (source_base='' AND source1='' AND source2!='' AND source3!='' AND source4='') OR
						  (source_base='' AND source1='' AND source2!='' AND source3='' AND source4!='') OR
						  (source_base='' AND source1='' AND source2='' AND source3!='' AND source4!='')"
			}
		}
		return "(" + query + ")"
	}
}

pub fn encode_type_dest(tag string) string {
	if tag == 'NOTHING' {
		return ""
	}
	else {
		mut words := []string
		mut word := ""
		for c in tag {
			if c.ascii_str() == ' '{
				words << word
				word = ""
			}
			else {
				word = word + c.ascii_str()
			}
		}
		words << word
		mut query := ""
		for w in words {
			if w == "||" {
				query = query + "OR "
			}
			else if w == "&" {
				query = query + "AND "
			}
			else if w == "hasCSR"{
				query = query + "dest_csr != ''"
			}
		}
		return "(" + query + ")"
	}
}

pub fn encode_type_imm(tag string) string {
if tag == 'NOTHING' {
		return ""
	}
	else {
		mut words := []string
		mut word := ""
		for c in tag {
			if c.ascii_str() == ' '{
				words << word
				word = ""
			}
			else {
				word = word + c.ascii_str()
			}
		}
		words << word
		mut query := ""
		for w in words {
			if w == "||" {
				query = query + "OR "
			}
			else if w == "&" {
				query = query + "AND "
			}
			else if w == "hasImm"{
				query = query + "base_imm != ''"
			}
		}
		return "(" + query + ")"
	}
}

pub fn encode_type_arch(tag string) ?string {
	mut app := App2 {
		database : sqlite.connect('instructionDB.db') or {panic(err)}
	}

	if tag == 'NOTHING' {
		return ""
	}
	else {
		parameter_eqn := "arch=" + "'" + tag + "'"
		records, _ := app.sel(parameter_eqn)
		if records.len == 0 {
			panic(tag + " does not exist in column arch")
		}
		return "(" + parameter_eqn + ")"
	}
}



// // encodes the tags of the field mnemonic
// // Facilitates intra-column unions (OR)
// // Does not allow intra-column intersections (AND)
// pub fn encode_mnemonic(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		column := "mnemonic"
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			if tag != "OR" || tag != "AND" {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				encoding = encoding + column + param_eqn
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the fields: function1, function2, function3
// // Facilitates intra-column unions (OR) & intersections (AND)
// pub fn encode_function(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		columns := ["function1", "function2", "function3"] // get from database directly ? for forward-compatibility
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "AND" || tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			else {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				for idx, c in columns {
// 					encoding = encoding + c + param_eqn
// 					if idx < columns.len-1{
// 						encoding = encoding + "OR "
// 					}
// 				}
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the fields: source1, source2, source3
// // Facilitates intra-column unions (OR) & intersections (AND)
// pub fn encode_source(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		columns := ["source1", "source2", "source3"]
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "AND" || tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			else {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				for idx, c in columns {
// 					encoding = encoding + c + param_eqn
// 					if idx < columns.len-1{
// 						encoding = encoding + "OR "
// 					}
// 				}
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the field destination
// // Facilitates intra-column unions (OR) 
// // Does not allow intra-column intersections (AND)
// pub fn encode_dest(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		column := "destination"
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			if tag != "OR" || tag != "AND" {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				encoding = encoding + column + param_eqn
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the fields function
// // Facilitates intra-column unions (OR) & intersections (AND)
// pub fn encode_imm(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		columns := ["imm1", "imm2", "imm3"]
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "AND" || tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			else {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				for idx, c in columns {
// 					encoding = encoding + c + param_eqn
// 					if idx < columns.len-1{
// 						encoding = encoding + "OR "
// 					}
// 				}
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the field destCSR
// // Facilitates intra-column unions (OR)
// // Does not allow intra-column intersections (AND)
// pub fn encode_dest_csr(tags []string) string {
// 	return '6'
// }

// // encodes the tags of the field arch
// // Facilitates intra-column unions (OR)
// // Does not allow intra-column intersections (AND)
// pub fn encode_arch(tag string) string {
// 	if tag == 'NOTHING'{
// 		return ""
// 	}
// 	else{
// 		return "arch=" + "'" + tag + "'" + " "
// 	}
// }

// // encodes the tags of the field ext
// // Facilitates intra-column unions (OR)
// // Does not allow intra-column intersections (AND)
// pub fn encode_ext(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		column := "ext"
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			if tag != "OR" || tag != "AND" {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				encoding = encoding + column + param_eqn
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

// // encodes the tags of the field exc
// // Facilitates intra-column unions (OR)
// // Does not allow intra-column intersections (AND)
// pub fn encode_exc(tags []string) string {
// 	if tags[0] == 'NOTHING' {
// 		return ""
// 	}
// 	else {
// 		column := "ext"
// 		mut encoding := ""
// 		for tag in tags {
// 			if tag == "OR" {
// 				encoding = encoding + tag + " "
// 			}
// 			if tag != "OR" || tag != "AND" {
// 				param_eqn := "=" + "'" + tag + "'" + " "
// 				encoding = encoding + column + param_eqn
// 			}
// 		}
// 		return "(" + encoding + ")"
// 	}
// }

pub fn create_database(){
	mut app := App2 {
		database : sqlite.connect('instructionDB.db') or {panic(err)}
	}

	sql app.database {
		create table DatabaseRecord
	}
}

pub fn (app &App2) find_all_records() []DatabaseRecord {
	return sql app.database {
		select from DatabaseRecord
	}
}


pub fn (app &App2) sel(query string) ([]sqlite.Row, int) {
	mut param := "SELECT * FROM DatabaseRecord WHERE " + query 
	println(param)
	return app.database.exec(param)
}

pub fn show_db(query string) {
	mut app := App2 {
		database : sqlite.connect('instructionDB.db') or {panic(err)}
	}
	mut records, _ := app.sel(query)
	println(records)
}