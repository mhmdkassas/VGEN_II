module db
import encoding.csv
import os
import sqlite

struct App {
	pub mut :
		database sqlite.DB
}

struct Opcode {
	id int 	[primary; sql:serial]
	mnemonic string
	architecture string
	function string
	size int
	exceptions string // will become []string
	format string
}

pub fn (app &App) find_all_entries() []Opcode {
	return sql app.database { 
		select from Opcode
	}
}

pub fn setup() {
	mut app := App {
		database : sqlite.connect('opcodes') or {panic(err)}
	}

	sql app.database {
		create table Opcode
	}
}

pub fn load_csv(filepath string) {
	content := os.read_file(filepath) or {panic(err)}
	mut reader := csv.new_reader(content)
	mut app := App {
		database : sqlite.connect('opcodes') or {panic(err)}
	}

	sql app.database {
		create table Opcode
	}

	reader.read() or {panic(err)}
	for {
        line_data := reader.read() or {
            break
        }
        fill_opcode(line_data)
    }
}

fn fill_opcode(line []string) {
	print(line)
	// Must fill Opcode 
	// Currently CSV file is not setup properly
	// Also calls insert_opcode
}

fn insert_opcode(app App, opcode Opcode) {
	sql app.database {
		insert opcode into Opcode
	}
}