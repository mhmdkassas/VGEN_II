module instr

pub struct Instruction {
	mnemonic string
	operands []string
}

// pub fn gen_instr() Instruction {}