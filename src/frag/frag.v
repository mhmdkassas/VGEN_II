module frag
import instr
import types

pub struct Fragment {
	instructions []instr.Instruction
}

pub struct FragmentDefinition {
	pub mut :
		definition_type string
		frag_name string = 'NONE'
		instr_categories []string = ['NONE']
		regs types.Regs
		targets types.Filters
		restrictions types.Filters
		blk_size int
}

// Frag Def :
// frag_name = RAW
// instr_categories = ['NONE']
// regs = RegMap
// targets = ['read', 'write']
// 

// // Gets pool of available instructions based on
// // criteria in fragment definition
// // Make sure that this will account for explicit instructions
// // Make sure that if db does return anything
// fn filter_instrs(fragdef FragmentDefinition) []instr.Instruction {}

// // Selects compatible instructions from the pool created by filter_instrs
// // Calls filter_instr
// fn select_instrs(fragdef FragmentDefinition) []instr.Instruction {}

// // Gets pool of available registers that are compatible with instruction
// fn get_avail_regs(fragdef FragmentDefinition) {}

// // Randomly selects from pool of registers
// // calls get_avail_regs()
// fn select_regs(fragdef FragmentDefinition, id int) {}

// // provided operand lists and instruction categories,
// // loop over all categories and call instr module
// // one instruction at a time
// // needs more parameters
// fn get_instr(instr instr.Instruction, operands map[string][]string) {} 

// // called by seq module and handles the two types of definitions for
// // fragment definition: implicit fragment or instruction-defined fragment
// pub fn gen_fragment(fragdef FragmentDefinition) Fragment {
// 	fragment := []instr.Instruction
// 	id_list := select_instrs(fragdef)
// 	for id in id_list {
// 		regs := select_regs(fragdef, id)
// 		instr := instr.gen_instr(id, regs)
// 		fragment << instr
// 	}
// 	return Fragment {
// 		instructions : fragment
// 	}
	

// }