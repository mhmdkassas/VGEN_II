module seq
import types
import frag

pub struct SequenceDefinition {
	pub mut :
		definition_type string
		seq_name string = 'NONE'
		frag_categories []string = ['NONE']
		instr_categories []string = ['NONE']
		regs types.Regs
		targets types.Filters
		restrictions types.Filters
		blk_size int
		interleavable bool
}

pub struct Sequence {
	pub mut :
		fragments []frag.Fragment
}

// Given sequence definition and in the case of an implicit sequence
// Generate the required fragment categories based on sequence block size
// and sequence category
fn select_fragment_categories(seqdef SequenceDefinition) {

}


// Generates all types of sequences and is called by test module
pub fn gen_sequence(seqdef SequenceDefinition) Sequence {
	fragdef := frag.FragmentDefinition{}
	seq := Sequence{}
	// All functions must fill a fragment definition
	// All functions must call frag.gen_fragment()
	// All functions must append to seq 
	if seqdef.seq_name != 'NONE' {
		// creates random fragments based on seq category and blk size
	} 
	else if seqdef.frag_categories != ['NONE'] {
		// creates explicit fragments 
	} 
	else if seqdef.instr_categories != ['NONE'] {
		// creates an explicit-instruction-defined fragment
	} 
	
	return Sequence{}
}