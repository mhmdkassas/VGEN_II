module main
import test
import db
import sqlite

fn main() {
	// users instantiates two structs that allow the use of the functions in test module
	mut test_maker := test.Test{}
	mut tools := test.Tools{}

	mut regs := tools.get_rand_reg_map(4, 2, 3, 1)?

	mut targets := tools.get_targets_filter(['NONE'], ['NONE'], ['NONE'], ['NONE'])

	mut restrictions := tools.get_restrictions_filter(['NONE'], ['NONE'], ['NONE'], ['NONE'])
	
	test_maker.add_implicit_seq('CTRL', 
							mut regs,
							mut targets,
							mut restrictions,
							5, 
							false)

							
	test_maker.add_implicit_seq('DATA', 
							mut regs,
							mut targets,
							mut restrictions,
							10, 
							true)

	test_maker.add_frag_defined_seq(['RAW', 'RMW', 'WAW'],
								   	mut regs,
									mut targets, 
									mut restrictions, 
									10,
									true)

	test_maker.add_instr_defined_seq(['READ', 'LOAD', 'STORE'],
								   	mut regs,
									mut targets, 
									mut restrictions, 
									10,
									true)
									
	// test_maker.add_op_defined_seq(['ADDI', 'MUL', 'LUI'],
	// 							   	mut regs,
	// 								mut targets, 
	// 								mut restrictions, 
	// 								10,
	// 								true)

	// mut f := db.FilterMap {
	// 	mnemonic_tags : ['LD', 'OR', 'ADDI']
	// 	logic1 : 'AND'
	// 	function_tags : ['alu', 'OR', 'cti']
	// }

	// q := f.encode()
	// db.show_db(q)
	// test.run(test_maker.test_definition)

	// f_map := db.FilterMap {
	// 	func_tags : "ld || cti"
	// 	logic2
		
	// }

	// q := f_map.encode()
	// print(q)
	// db.show_db(q)

	// mut app := db.App2 {
	// 	database : sqlite.connect('instructionDB.db') or {panic(err)}
	// }

	query := "(func = 'ld' AND size='word') OR (func='st' AND size='byte')"
	db.show_db(query)



}