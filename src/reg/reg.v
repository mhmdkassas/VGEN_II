module reg

pub type Regs = RegMap | RandRegMap

pub struct RegMap {
	pub :
		source []string
		dest []string
		source_dest []string
		base []string
}

pub struct RandRegMap {
	pub :
		source int
		dest int
		source_dest int
		base int
}
// pub fn randomize_regs(mut regs map[string]int) regs map[string]string{
// 	// Parse database of regs
// 	// select regs randomly
// 	// return map of keys (source, dest, source_dest, and base) and values (selected regs for each category)
// }