module test
import seq
import filter
import reg
import types

pub struct Test {
	pub mut :
		test_definition []seq.SequenceDefinition
}

pub struct Tools {

}

// M1
// A method of Tools{} that returns a reg.RandRegMap struct
// This struct indicates the number of random registers 
// desired for each of source, dest, source_dest, and base
pub fn (tool Tools) get_rand_reg_map(num_source int, num_dest int, num_both int, num_base int) ?reg.RandRegMap {
	s := num_source + num_dest + num_both + num_base
	if s == 0 {
		return error('All registers are set to 0. At least one of the register categories needs to be non-zero')
	}
	else {
		return reg.RandRegMap {
			source : num_source
			dest : num_dest
			source_dest : num_both
			base : num_base
		}
	}
}

// M2
// A method of Tools{} that returns a reg.RegMap struct
// Thi struct explicitly specifies the desired registers 
// for each of sourvce, dest, sourcce_dest, and base
pub fn (tool Tools) get_reg_map(sources []string, dests []string, sources_dests []string, base []string) ?reg.RegMap {
	if	sources == [''] && dests == [''] && sources_dests == [''] && base == [''] {
		return error('Explicit registers cannot be empty')
	}
	else {
		return reg.RegMap {
			source : sources
			dest : dests
			source_dest : sources_dests
			base : base	
		}
	}
}

// M3
// A method of Tools{} that returns a filter.Targets struct
// This struct holds arrays of tags that will specify desired 
// sequence, fragment, instruction, and opcode categories 
pub fn (tool Tools) get_targets_filter(seq_tags []string, frag_tags []string, instr_tags []string, op_tags []string) filter.Target {
	return filter.Target {
		seq : seq_tags
		frag : frag_tags
		instr : instr_tags
		op : op_tags	
	}
}

// M4
// A method of Tools{} that returns a filter.Targets struct
// This struct holds arrays of tags that will specify restricted 
// sequence, fragment, instruction, and opcode categories 
pub fn (tool Tools) get_restrictions_filter(seq_tags []string, frag_tags []string, instr_tags []string, op_tags []string) filter.Restriction {
	return filter.Restriction {
		seq : seq_tags
		frag : frag_tags
		instr : instr_tags
		op : op_tags	
	}
}

// M5
// A method of Test{} that fills a seq.SequenceDefinition
// and appends it to the array test_definition
// This method keeps the default values of the fields
// frag_categories, instr_categories, and opcodes
pub fn (mut t Test) add_implicit_seq(
										category string, 
										mut regs types.Regs, 
										mut targets types.Filters,
										mut restrictions types.Filters,
										blk_size int, 
										interleavable bool
									) {
	
	
	t.test_definition << seq.SequenceDefinition {
												definition_type : 'IMPLICIT',
												seq_name : category,
												regs : regs
												targets : targets
												restrictions : restrictions
												blk_size : blk_size,
												interleavable : interleavable
												}
}

// M6
// A method of Test{} that fills a seq.SequenceDefinition
// and appends it to the array test_definition
// This method keeps the default values of the fields
// category, instr_categories, and opcodes
pub fn (mut t Test) add_frag_defined_seq(
										frag_categories []string, 
										mut regs types.Regs, 
										mut targets types.Filters,
										mut restrictions types.Filters,
										blk_size int, 
										interleavable bool
									) {
	
	
	t.test_definition << seq.SequenceDefinition {
												definition_type : 'FRAG-DEF EXPLICIT',
												frag_categories : frag_categories,
												regs : regs
												targets : targets
												restrictions : restrictions
												blk_size : blk_size,
												interleavable : interleavable
												}
}

// M7
// A method of Test{} that fills a seq.SequenceDefinition
// and appends it to the array test_definition
// This method keeps the default values of the fields
// category, frag_categories, and opcodes
pub fn (mut t Test) add_instr_defined_seq(
										instr_categories []string, 
										mut regs types.Regs, 
										mut targets types.Filters,
										mut restrictions types.Filters,
										blk_size int, 
										interleavable bool
									) {
	
	
	t.test_definition << seq.SequenceDefinition {
												definition_type : 'INSTR-DEF EXPLICIT',
												instr_categories : instr_categories,
												regs : regs
												targets : targets
												restrictions : restrictions
												blk_size : blk_size,
												interleavable : interleavable
												}
}

// M8
// A method of Test{} that fills a seq.SequenceDefinition
// and appends it to the array test_definition
// This method keeps the default values of the fields
// category, frag_categories, and instr_categories
// pub fn (mut t Test) add_op_defined_seq(
// 										opcodes []string, 
// 										mut regs types.Regs, 
// 										mut targets types.Filters,
// 										mut restrictions types.Filters,
// 										blk_size int, 
// 										interleavable bool
// 									) {
	
	
// 	t.test_definition << seq.SequenceDefinition {
// 												definition_type : 'OP-DEF EXPLICIT',
// 												opcodes : opcodes,
// 												regs : regs
// 												targets : targets
// 												restrictions : restrictions
// 												blk_size : blk_size,
// 												interleavable : interleavable
// 												}
// }


pub fn run(test_definition []seq.SequenceDefinition) {
	mut seqs := []seq.Sequence{}
	all_seq_definitions := test_definition.clone()
	for definition in all_seq_definitions {
		seq.gen_sequence(definition)
	}
}