= VGEN II Documentation
:toc:

== Introduction
VGEN II is the second iteration of a self-checking and coverage-driven random instruction generator. It provides the user with the ability to construct fully directed, fully randomized or rando-directed tests. VGEN II is able to provide that continual range of abstraction & granularity to the user due to the heirarchal organization of one test. A test is comprised of sequences, which consist of fragments, which are made up of instructions made up of opcodes and operands. 

image::heirarchy_2.png[align="center"]
image::heirarchy.png[align = "center"]

The user is responsible for defining the sequences of the test using the structs and methods found in the test module. After defining all desired sequences, the user can set the boolean field interleave in the test module. If set to true, VGEN will generate helixes. A helix is made up of two or more interleaved sequences. A helix maintains the intra-sequence order of fragments.

image::two_seqs.png[1000, 500, align = "center"]
image::helix.png[200, 200, align = "center"]


== Abstract and Terminology
VGEN provides the ability to create a test by defining sequences. 

.Sequences can be defined as: +
* implicit 
* explicit fragment-defined 
* explicit instruction-defined 
* explicit opcode-defined 

An *implicit* entity is one that is defined using a category applicable to the entity. +
An *explicit* entity is one that is defined using one or more components that are implicitly or explicitly defined. +
A *random* entity is one that is to be selected randomly from a pool based on its parent entity category. +

image::entity_types.png[align="center"]

The granularity of randomization can be showed in the table below. +

// image::randomization_gran.png[]

== VGEN Software Architecture
From a software perspective, VGEN is made up of modules, which contain structs and functions.

A 'Test' is a V structure containing the test definition and sequences list. Struct 'test' is located in the test module. +

A 'Sequence' is a V structure containing the sequence definition and fragment list. Struct 'sequence' is lcoated in the seq module. +

A 'Fragment' is a V structure containing the fragment defintion and instruction list. Struct 'fragment' is locate in the FragMod module. +

An 'Instruction' is a V structure containing the instruction definition and the opcode and operands. +

An 'Opcode' is an object stored in an SQLite database whose fields house information about the opcode (mnemonic, architecture, format type, exception types, and size). + 

An 'Operands' is a dictionary of registers and immediates. Based on the format type of the opcode, the operand values will be set appropriately. Fields that are not applicable to format type are set to 'NA'.

.Available Modules:
* test
* seq
* frag
* instr
* reg
* log

=== 'test' Module
The test module is a collection of structs and methods that allow the user to define a test based on its constituents. A test's constituents are the sequences, whose constituents are fragments, whose constituents are instructions, and finally whose constituents are opcodes and oeprands. This is derived from the heirarchal organization mentioned in the Introduction section. To be able to provide full range of randomization and direction, the test module provides the user with two structs (test.Tools{} and test.Test{}) and their corresponding methods. The user defines sequences to make up the test using the methods of the Tools{} struct. Every sequence requires ......

.Test{} Methods:
* addImplicitSeq(...)
* addFragDefinedSeq(...)
* addInstrDefinedSeq(...)
* addOpDefinedSeq(...)
* run()

.Tool{} Methods:
* get_rand_reg_map(...)
* get_reg_map(...)
* get_targets_filter(...)
* get_restrictions_filter(...)

==== addImplicitSeq(string category, map[][]string{} regs, map[][]string{} targets, map[][]string{} restrictions, int blk_size, bool interleavable)     
.inputs:
* category specifies the category of the sequence (e.x. 'CTRL_HAZARD')
* regs specifies a dictionary of explicit registers to select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* blk_size specifies the number of fragments per sequence
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence

==== addImplicitSeq(string category, map[]int{} regs, map[][]string{} targets, map[][]string{} restrictions, int blk_size, bool interleavable)     
.inputs:
* category specifies the category of the sequence (e.x. 'CTRL_HAZARD')
* regs specifies a number of registers to randomize and select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* blk_size specifies the number of fragments per sequence
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence  

==== addFragDefinedSeq([]string categories, map[][]string{} regs, map[][]string{} targets, map[][]string{} restrictions, int blk_size, bool interleavable)     
.inputs:
* category specifies the categories of the fragments comprising the sequence (e.x. ['RAW', 'RMW', 'WRITE'])
* regs specifies a dictionary of explicit registers to select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* blk_size specifies the number of instructions per fragment
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

==== addFragDefinedSeq([]string categories, map[]int{} regs, map[][]string{} targets, map[][]string{} restrictions, int blk_size, bool interleavable)     
.inputs:
* category specifies the categories of the fragments comprising the sequence (e.x. ['RAW', 'RMW', 'WRITE'])
* regs specifies a number of registers to randomize and select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* blk_size specifies the number of instructions per fragment
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

==== addInstrDefinedSeq([]string categories, map[][]string{} regs, map[][]string{} targets, map[][]string{} restrictions, bool interleavable)     
.inputs:
* category specifies the categories of the instructions comprising the sequence (e.x. ['LOADWORD', 'LOADBYTE', 'READBYTE'])
* regs specifies a dictionary of explicit registers to select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

==== addInstrDefinedSeq([]string categories, map[]int{} regs, map[][]string{} targets, map[][]string{} restrictions, bool interleavable)     
.inputs:
* category specifies the categories of the instructions comprising the sequence (e.x. ['RAW', 'RMW', 'WRITE'])
* regs specifies a number of registers to randomize and select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

==== addOpDefinedSeq([]string categories, map[][]string{} regs, map[][]string{} targets, map[][]string{} restrictions, bool interleavable)     
.inputs:
* category specifies the categories of the opcodes comprising the sequence (e.x. ['1011', '1101', '1111'])
* regs specifies a dictionary of explicit registers to select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

==== addOpDefinedSeq([]string categories, map[]int{} regs, map[][]string{} targets, map[][]string{} restrictions, bool interleavable)     
.inputs:
* category specifies the categories of the instructions comprising the sequence (e.x. ['1011', '1101', '1111'])
* regs specifies a number of registers to randomize and select from
* targets indicate one or more desired architectural, functional, and/or granular tests
* interleavable is the flag that indicates whether the sequence can be interleaved with another interleavable sequence 

=== db module
The db module houses the methods and structs that allow VGEN to create a database and parse through it

This module must provide functions that are able to extract rows based on unions and intersections of conditions. +
e.g. +
I want rows with col1=x AND col2=y AND ... (Inter-Column Intersection) +
I want rows with col1=x OR col2=y OR ... (Inter-Column Union) +
I want rows with col1=x AND col1=y AND ... (Intra-Column Intersection) +
I want rows with col1=x OR col1=y OR ... (Intra-Column Union) +

Inter-column intersection conditions are applicable to any of the fields +
Inter-column union conditions are applicable to any of the fields +
Intra-Column intersection conditions are applicable to any of the fields +
Intra-column union conditions are only applicable for 'function', 'source', 'destination', 'imm', and 'destCSR'

condition example --> string +
'(mnemonic=LB OR LH OR ANDI) OR (function=alu)'